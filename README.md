
# Patched

- [actualfullscreen](https://dwm.suckless.org/patches/actualfullscreen/)
- [attachbottom](https://dwm.suckless.org/patches/attachbottom/)
- [gaplessgrid](https://dwm.suckless.org/patches/gaplessgrid/)
- [ispermanent](https://dwm.suckless.org/patches/ispermanent/)
- [movestack](https://dwm.suckless.org/patches/movestack/)
- [pertag](https://dwm.suckless.org/patches/pertag/)
- [scratchpad](https://dwm.suckless.org/patches/scratchpad/)
- [stackmfact](https://dwm.suckless.org/patches/stackmfact/): to be improved
- [statuscmd](https://dwm.suckless.org/patches/statuscmd/): to be used
- [sticky](https://dwm.suckless.org/patches/sticky/): modified to include in the rules

# Features

- disabled mouse focus change
- tweaked some parameters in configuration

# Dependencies

- `libxft-bgra`: [install it from source](https://github.com/uditkarode/libxft-bgra)
- `libxcb`
- `Xlib-libxcb`
- `xcb-res`

# Inserting emojis and colored characters

This is a two-step solution:

1. Install `libxft-bgra`
2. Remove the default workaround in source code (`grep iscol *`)

# statuscmd usage

By clicking in the status bar icon, it calls the script referenced in
`config.h`. A signal called `BUTTON` is emitted. E.g.:

```sh
#!/bin/sh
case $BUTTON in
	1) notify-send "CPU usage" "$(ps axch -o cmd,%cpu --sort=-%cpu | head)";;
	3) st -e htop;;
esac
```

`BUTTON` = 1 is left-click; 2, right click?

Be careful when assigning signals. E.g.: new line is "10" (=`\x0a`).

# Running dwm

Add the following line to your .xinitrc to start dwm using startx:

```sh
exec dwm
```

In order to connect dwm to a specific display, make sure that
the DISPLAY environment variable is set correctly, e.g.:

```sh
DISPLAY=foo.bar:1 exec dwm
```

This will start dwm on display `:1` of the host `foo.bar`.
