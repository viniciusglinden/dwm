/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int minwsz    = 5;       /* Minimal height of a client for smfact */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "monospace:size=10",
                                        "JoyPixels:pixelsize=10:antialias=true:autohint=true" };
static const char dmenufont[]       = "monospace:size=10";
static const char normbgcolor[]     = "#222222"; // #222222
static const char normbordercolor[] = "#444444"; // #444444
static const char normfgcolor[]     = "#bbbbbb"; // #bbbbbb
static const char selfgcolor[]      = "#eeeeee"; // #eeeeee
static const char selbordercolor[]  = "#770000"; // #770000
static const char selbgcolor[]      = "#aa7777"; // #aa7777
static const char *colors[][3]      = {
/*  mode            fg              bg              border   */
    [SchemeNorm] = {normfgcolor,    normbgcolor,    normbordercolor},
    [SchemeSel]  = {selfgcolor,     selbgcolor,     selbordercolor},
};

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbgcolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char scratchpadname[] = "python";
static const char *scratchpadcmd[] = { "st", "-n", scratchpadname, "-t", scratchpadname, "-f", "monospace:size=14", "-g", "100x30", "-e", "bpython", "-q", NULL };

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };


static const Rule rules[] = {
    /* xprop(1):
     *  WM_CLASS(STRING) = instance, class
     *  WM_NAME(STRING) = title
     */
    /* class      instance              title                   tags mask   isfloating    ispermanent monitor   issticky */
    { "St",       scratchpadname,       scratchpadname,         0,          1,            1,          -1,       1 },
    //{ "Skype",    "skype",              "Skype Preview",        0,          1,            0,          -1,       0 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const float smfact    = 0.00; /* factor of vertically tiled clients [0.00..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

#include "movestack.c"
#include "gaplessgrid.c"
static const Layout layouts[] = {
    /* symbol     arrange function */
    { "[]=",      tile },               // 0 tiled (multiple neightboring clients)
    { "><>",      NULL },               // 1 floating (floating client)
    { "[M]",      monocle },            // 2 monocle
    { "###",      gaplessgrid },        // 3 grid
};
/*
 * NOTE: to retile a window just press Super+middle mouse button
 */

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
    { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }
/* spawn in terminal */
#define TERMINAL(cmd) { .v = (const char*[]){ "st", "-e", cmd, NULL } }

// regular symbols are defined at /usr/include/X11/keysymdef.h
// special symbols are defined at /usr/include/X11/XF86keysym.h
#include <X11/XF86keysym.h>

static Key keys[] = {
    /* modifier             key                         function        argument */
    // displays every open client
    { MODKEY,               XK_0,                       view,           {.ui = ~0 } },
    // show current client in every tag
    { MODKEY|ShiftMask,     XK_0,                       tag,            {.ui = ~0 } },
    // shutdown, reboot, hibernate, etc
    { MODKEY,               XK_Escape,                  spawn,          SHCMD("sysact") },
    // locks the screen with password
    { MODKEY,               XK_x,                       spawn,          SHCMD("slock & xset dpms force off; mpc pause ; pauseallmpv") },
    // alternate tags
    { MODKEY,               XK_Tab,                     view,           {0} },
    // exit client
    { MODKEY,               XK_q,                       killclient,     {0} },
    // full screen
    { MODKEY,               XK_F11,                     togglefullscr,  {0} },
    // opens browser
    { MODKEY,               XK_w,                       spawn,          SHCMD("$BROWSER") },
    // opens Qutebrowser
    { MODKEY,               XK_b,                       spawn,          SHCMD("qutebrowser") },
    // opens alternative browser
    { MODKEY|ShiftMask,     XK_b,                       spawn,          SHCMD("$BROWSER2") },
    // calls dmenuvim script
    { MODKEY,               XK_v,                       spawn,          SHCMD("dmenuvim") },
    // calls dmenuopen script
    { MODKEY|ShiftMask,     XK_v,                       spawn,          SHCMD("dmenuopen") },
    // opens Inkscape
    { MODKEY,               XK_i,                       spawn,          SHCMD("inkscape") },
    /* open wiki */
    { MODKEY,               XK_u,                       spawn,          TERMINAL("wiki") },
    // opens wifi connector assistance
    { MODKEY|ShiftMask,     XK_w,                       spawn,          SHCMD("$CONNECTOR_ASSISTANT") },
    // change layout (see above)
    { MODKEY,               XK_t,                       setlayout,      {.v = &layouts[0]} },
    { MODKEY,               XK_f,                       setlayout,      {.v = &layouts[1]} },
    { MODKEY,               XK_m,                       setlayout,      {.v = &layouts[2]} },
    { MODKEY,               XK_g,                       setlayout,      {.v = &layouts[3]} },
    // spawns the scratchpad
    { MODKEY,               XK_s,                       togglescratch,  {.v = scratchpadcmd } },
    // file manager
    { MODKEY,               XK_r,                       spawn,          TERMINAL("lfub") },
    // horizontal mode
    { MODKEY,               XK_o,                       incnmaster,     {.i = +1 } },
    // vertical mode
    { MODKEY|ShiftMask,     XK_o,                       incnmaster,     {.i = -1 } },
    // change stack order
    { MODKEY|ShiftMask,     XK_j,                       movestack,      {.i = +1 } },
    { MODKEY|ShiftMask,     XK_k,                       movestack,      {.i = -1 } },
    // change focus
    { MODKEY,               XK_j,                       focusstack,     {.i = +1 } },
    { MODKEY,               XK_k,                       focusstack,     {.i = -1 } },
    // move vertical separator to the left
    { MODKEY,               XK_h,                       setmfact,       {.f = -0.05} },
    // move vertical separator to the right
    { MODKEY,               XK_l,                       setmfact,       {.f = +0.05} },
    // move horizontal separator up
    { MODKEY|ControlMask,   XK_k,                       setsmfact,      {.f = +0.05} },
    // move horizontal separator down
    { MODKEY|ControlMask,   XK_j,                       setsmfact,      {.f = -0.05} },
    // opens volume mixer
    { MODKEY,               XK_a,                       spawn,          TERMINAL("pulsemixer") },
    // opens alsa volume mixer
    { MODKEY|ShiftMask,     XK_a,                       spawn,          TERMINAL("alsamixer") },
    // dmenu command
    { MODKEY,               XK_d,                       spawn,          {.v = dmenucmd } },
    // calls dmenuopen under Documents folder
    { MODKEY|ShiftMask,     XK_d,                       spawn,          SHCMD("DMENUOPEN_FOLDER=~/Documents dmenuopen") },
    // spawns a terminal in the current directory
    { MODKEY,               XK_Return,                  spawn,          SHCMD("samedir") },
    // spawns a terminal
    { MODKEY|ShiftMask,     XK_Return,                  spawn,          {.v = termcmd } },
    // locks the screen with password
    //{ MODKEY,             XK_x,                       spawn,          SHCMD("slock & xset dpms force off; mpc pause ; pauseallmpv") },
    // opens-up webcam window
    { MODKEY,               XK_c,                       spawn,          SHCMD("camtoggle") },
    // shows contents of clipboard
    { MODKEY,               XK_Insert,                  spawn,          SHCMD("notify-send \"📋 Clipboard:\" \" $(xclip -o -selection clipboard)\"") },
    // opens-up help PDF
    { MODKEY,               XK_F1,                      spawn,          SHCMD("zathura /usr/local/share/dwm/help.pdf") },
    // get emojis
    { MODKEY,               XK_F2,                      spawn,          SHCMD("dmenuunicode") },
    // selects display
    { MODKEY,               XK_F3,                      spawn,          SHCMD("displayselect") },
    // change keyboard layout
    { MODKEY,               XK_F4,                      spawn,          SHCMD("keyboard-load") },
    // reload keyboard layout
    { MODKEY|ShiftMask,     XK_F4,                      spawn,          SHCMD("keyboard-load default") },
    // open notes
    { MODKEY,               XK_F5,                      spawn,          SHCMD("cnote -o") },
    // compiles & open notes
    { MODKEY|ShiftMask,     XK_F5,                      spawn,          SHCMD("cnote") },
    /* start some type of recording */
    { MODKEY,               XK_F6,                      spawn,          SHCMD("dmenurecord") },
    // toggles torrent daemon
    { MODKEY,               XK_F7,                      spawn,          SHCMD("td-toggle") },
    // interactive mount script
    { MODKEY,               XK_F9,                      spawn,          SHCMD("dmenumount") },
    // interactive unmount script
    { MODKEY|ShiftMask,     XK_F9,                      spawn,          SHCMD("dmenuumount") },
    // make the current client the master client
    { MODKEY,               XK_space,                   zoom,           {0} },
    // float current client
    { MODKEY|ShiftMask,     XK_space,                   togglefloating, {0} },
    // prints whole screen to a .png file
    { 0,                    XK_Print,                   spawn,          SHCMD("maim pic-full-$(date '+%y%m%d-%H%M-%S').png") },
    // queries user to select a printscreen mode
    { ShiftMask,            XK_Print,                   spawn,          SHCMD("maimpick") },
    { MODKEY,               XK_Scroll_Lock,             spawn,          SHCMD("killall screenkey || screenkey &") },
    // mute audio
    { MODKEY|ShiftMask,     XK_m,                       spawn,          SHCMD("lmc toggle") },
    // toggles audio
    { 0,                    XF86XK_AudioMute,           spawn,          SHCMD("lmc toggle") },
    // increases audio volume
    { 0,                    XF86XK_AudioRaiseVolume,    spawn,          SHCMD("lmc up 2") },
    // decreases audio volume
    { 0,                    XF86XK_AudioLowerVolume,    spawn,          SHCMD("lmc down 2") },
    // track toggle
    { MODKEY,               XK_p,                       spawn,          SHCMD("mpc toggle") },
    // track pause
    { MODKEY|ShiftMask,     XK_p,                       spawn,          SHCMD("mpc pause ; pauseallmpv") },
    // track forward
    { MODKEY,               XK_bracketleft,             spawn,          SHCMD("mpc seek -5") },
    // track forward plus
    { MODKEY|ShiftMask,     XK_bracketleft,             spawn,          SHCMD("mpc seek -120") },
    // track backward
    { MODKEY,               XK_bracketright,            spawn,          SHCMD("mpc seek +5") },
    // track backward minus
    { MODKEY|ShiftMask,     XK_bracketright,            spawn,          SHCMD("mpc seek +120") },
    // previous track
    { 0,                    XF86XK_AudioPrev,           spawn,          SHCMD("mpc prev") },
    { MODKEY,               XK_comma,                   spawn,          SHCMD("mpc prev") },
    // next track
    { 0,                    XF86XK_AudioNext,           spawn,          SHCMD("mpc next") },
    { MODKEY|ShiftMask,     XK_period,                  spawn,          SHCMD("mpc next") },
    // toggle track
    { 0,                    XF86XK_AudioPause,          spawn,          SHCMD("mpc pause") },
    // play track (same key as AudioPause in my laptop)
    //{ 0,                  XF86XK_AudioPlay,           spawn,          SHCMD("mpc play") },
    { 0,                    XF86XK_AudioPlay,           spawn,          SHCMD("mpc toggle") },
    // stop track
    { 0,                    XF86XK_AudioStop,           spawn,          SHCMD("mpc stop") },
    // rollsback track
    { 0,                    XF86XK_AudioRewind,         spawn,          SHCMD("mpc seek -5") },
    // forwards track
    { 0,                    XF86XK_AudioForward,        spawn,          SHCMD("mpc seek +5") },
    // shows the music daemon front-end
    { 0,                    XF86XK_AudioMedia,          spawn,          TERMINAL("ncmpcpp") },
    { MODKEY,               XK_n,                       spawn,          TERMINAL("ncmpcpp") },
    // opens up neomutt
    { MODKEY|ShiftMask,     XK_n,                       spawn,          TERMINAL("neomutt") },
    // track restart
    { MODKEY|ShiftMask,     XK_comma,                   spawn,          SHCMD("mpc seek 0%") },
    // shutsdown computer
    { 0,                    XF86XK_PowerOff,            spawn,          SHCMD("sysact") },
    // calculator
    { 0,                    XF86XK_Calculator,          togglescratch,  {.v = scratchpadcmd } },
    // hibernates computer
    { 0,                    XF86XK_Sleep,               spawn,          SHCMD("sysact") },
    // opens-up the browser
    { 0,                    XF86XK_WWW,                 spawn,          SHCMD("$BROWSER") },
    // opens-up the terminal
    { 0,                    XF86XK_DOS,                 spawn,          SHCMD("st") },
    // locks the screen and stops all media
    { 0,                    XF86XK_ScreenSaver,         spawn,          SHCMD("slock & xset dpms force off; mpc pause; pauseallmpv") },
    // opens a terminal, brings htop
    { 0,                    XF86XK_TaskPane,            spawn,          TERMINAL("htop") },
    // opens file explorer at root
    { 0,                    XF86XK_MyComputer,          spawn,          TERMINAL("lf") },
    // shutsdown screen??
    { 0,                    XF86XK_Launch1,             spawn,          SHCMD("xset dpms force off") },
    // toggles touchpad (not working)
    { 0,                    XF86XK_TouchpadToggle,      spawn,          SHCMD("(synclient | grep 'TouchpadOff.*1' && synclient TouchpadOff=0) || synclient TouchpadOff=1") },
    // enables touchpad (not working)
    { 0,                    XF86XK_TouchpadOff,         spawn,          SHCMD("synclient TouchpadOff=1") },
    // disables touchpad (not working)
    { 0,                    XF86XK_TouchpadOn,          spawn,          SHCMD("synclient TouchpadOff=0") },
    // increases screen brightness
    { 0,                    XF86XK_MonBrightnessUp,     spawn,          SHCMD("brightness +100") },
    // decreases screen brightness
    { 0,                    XF86XK_MonBrightnessDown,   spawn,          SHCMD("brightness -100") },
    /* send client to right monitor */
    { MODKEY|ShiftMask,     XK_slash,                   tagmon,         {.i = -1 } },
    /* send client to left monitor */
    { MODKEY|ShiftMask,     XK_semicolon,               tagmon,         {.i = +1 } },
    /* focus on the right monitor */
    { MODKEY,               XK_slash,                   focusmon,       {.i = -1 } },
    /* focus on the left monitor */
    { MODKEY,               XK_semicolon,               focusmon,       {.i = +1 } },
    TAGKEYS( XK_1, 0)
    TAGKEYS( XK_2, 1)
    TAGKEYS( XK_3, 2)
    TAGKEYS( XK_4, 3)
    TAGKEYS( XK_5, 4)
    TAGKEYS( XK_6, 5)
    TAGKEYS( XK_7, 6)
    TAGKEYS( XK_8, 7)
    TAGKEYS( XK_9, 8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function        argument */
    { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
    { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
    { ClkWinTitle,          0,              Button2,        zoom,           {0} },
    { ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
    { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
    { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
    { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
    { ClkTagBar,            0,              Button1,        view,           {0} },
    { ClkTagBar,            0,              Button3,        toggleview,     {0} },
    { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
    { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

