# dwm - dynamic window manager
# See LICENSE file for copyright and license details.

include config.mk

SRC = drw.c dwm.c util.c
OBJ = ${SRC:.c=.o}

all: options dwm

options:
	@echo dwm build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h config.mk

dwm: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f dwm ${OBJ} dwm-${VERSION}.tar.gz *.orig *.rej *.pdf help.md

dist: clean
	mkdir -p dwm-${VERSION}
	cp -R LICENSE Makefile README config.mk\
		dwm.1 drw.h util.h ${SRC} dwm.png transient.c dwm-${VERSION}
	tar -cf dwm-${VERSION}.tar dwm-${VERSION}
	gzip dwm-${VERSION}.tar
	rm -rf dwm-${VERSION}
	rm -rf help.md

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f dwm ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/dwm
	mkdir -p ${DESTDIR}${PREFIX}/share/dwm
	rm -rf help.md
	echo "\`\`\`c" >> help.md
	cat config.h >> help.md
	echo "\`\`\`" >> help.md
	# TODO fix pdf compilation
	# pandoc help.md -V geometry:margin=1mm --pdf-engine=xelatex -s -o help.pdf
	# cp -f help.pdf ${DESTDIR}${PREFIX}/share/dwm
	# chmod 644 ${DESTDIR}${PREFIX}/share/dwm/help.pdf

uninstall:
	rm -rf ${DESTDIR}${PREFIX}/bin/dwm\
		${DESTDIR}${PREFIX}/help.pdf

.PHONY: all options clean dist install uninstall
